# Django settings for report_ui project.

DEBUG = True
TEMPLATE_DEBUG = DEBUG

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
PROJECT_PATH = os.path.dirname(os.path.dirname(__file__))

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'ENGINE': 'django.db.backends.mysql',
        # Or path to database file if using sqlite3.
        'NAME': 'DNSLogDB',
        #'DNSLogReportingDB',
        # The following settings are not used with sqlite3:
        'USER': 'root',
        'PASSWORD': 'SmarTek21',
        # Empty for localhost through domain sockets or '127.0.0.1'
        # for localhost through TCP.
        'HOST': '192.168.50.32',
        # Set to empty string for default.
        'PORT': '',
    }
}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = []

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'America/Chicago'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

## URL that handles the media served from MEDIA_ROOT. Make sure to use a
## trailing slash.
## Examples: "http://example.com/media/", "http://media.example.com/"
#MEDIA_URL = ''
MEDIA_ROOT = os.path.join(PROJECT_PATH, "media")
MEDIA_URL = "/media/"

## Absolute path to the directory static files should be collected to.
## Don't put anything in this directory yourself; store your static files
## in apps' "static/" subdirectories and in STATICFILES_DIRS.
## Example: "/var/www/example.com/static/"
#STATIC_ROOT = ''

## URL prefix for static files.
## Example: "http://example.com/static/", "http://static.example.com/"
#STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(PROJECT_PATH, "static")
STATIC_URL = "/static/"

# Additional locations of static files
STATICFILES_DIRS = (os.path.join(PROJECT_PATH, "media"),
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'm)jg%h%kqvinjqklx)v=lj9#g6jmx$2pl%4-(u97poqn)fod=)'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'report_ui.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'report_ui.wsgi.application'

TEMPLATE_DIRS = (os.path.join(PROJECT_PATH, "templates"),
    # Put strings here, like "/home/html/django_templates"
    # or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    'dnslog'
)

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'


IMPALA_API_URL = 'http://127.0.0.1:8080/'

API_URL_REPORT_DATE = 'getReportDate'
API_URL_HASHTAGSADIDAS = 'gethashtagsadidas'
API_URL_HASHTAGSNIKE = 'gethashtagsnike'
API_URL_HASHTAGSREEBOK = 'gethashtagsreebok'
API_URL_WORDCLOUDADIDAS = 'getwordcloudadidas'
API_URL_WORDCLOUDNIKE = 'getwordcloudnike'
API_URL_WORDCLOUDREEBOK = 'getwordcloudreebok'
API_URL_TWITTERSENTIMENT = 'twitterzoomabletree'
API_URL_RETWEET = 'getretweets'

# service machines (or) common machines
NOMAN_IPS = ['192.168.50.22', '192.168.1.53', '192.168.50.31',
             '192.168.0.20', '192.168.0.21', '192.168.0.15',
             '192.168.50.20', '192.168.0.48', '192.168.0.24',
             '192.168.0.61', '192.168.10.15', '192.168.0.16',
             '192.168.8.31', '192.168.8.1', '192.168.0.55',
             '192.168.0.53', '192.168.0.40', '192.168.0.91',
             '192.168.0.74', '192.168.0.59', '192.168.0.118',
             '192.168.1.24', '192.168.0.47', '192.168.0.27',
             '192.168.0.72', '192.168.0.26', '192.168.0.68',
             '192.168.1.0', '192.168.1.46', '192.168.1.89',
             '192.168.0.202', '192.168.0.63', '192.168.0.54',
             '192.168.0.57', '192.168.1.57', '192.168.0.65',
             '192.168.0.73', '192.168.0.44', '192.168.1.71',
             '192.168.0.56', '192.168.0.23', '192.168.1.77',
             '192.168.0.75', '192.168.0.45', '192.168.0.66',
             '192.168.0.117', '192.168.0.62', '192.168.1.72',
             '192.168.0.70', '192.168.0.112', '192.168.0.43',
             '192.168.0.52', '192.168.2.175', '192.168.50.206',
             '192.168.0.29', '192.168.0.71', '192.168.50.182',
             '192.168.10.228', '192.168.8.81', '192.168.8.82',
             '192.168.8.75', '192.168.8.8', '192.168.8.71',
             '192.168.8.86', '192.168.8.150', '192.168.11.49',
             '192.168.8.91', '192.168.10.211', '192.168.8.40',
             '192.168.1.38', '192.168.8.25', '192.168.1.69',
             '192.168.8.210', '192.168.10.56', '192.168.10.142',
             '192.168.1.10', '192.168.1.25', '192.168.8.65',
             '192.168.10.13', '192.168.1.60', '192.168.10.209',
             '192.168.199.10', '192.168.8.12', '192.168.8.33',
             '192.168.50.23', '192.168.8.26', '192.168.8.111',
             '192.168.8.175', '192.168.8.45', '192.168.0.51',
             '192.168.8.66', '192.168.8.165', '192.168.16.83',
             '192.168.10.239', '192.168.10.187', '192.168.10.20',
             '192.168.10.139', '192.168.10.184', '192.168.10.188',
             '192.168.10.202', '192.168.1.82', '192.168.11.28',
             '192.168.10.243', '192.168.50.72', '192.168.9.130',
             '192.168.50.113', '192.168.9.123', '192.168.9.125',
             '192.168.10.158', '192.168.11.12', '192.168.10.104',
             '192.168.10.240', '192.168.50.102', '192.168.0.31',
             '192.168.0.123', '192.168.2.176', '192.168.0.123',
             '192.168.0.31', '192.168.50.102', '192.168.10.240',
             '192.168.11.39', '192.168.11.47', '192.168.9.124',
             '192.168.9.128', '192.168.10.234', '192.168.9.129',
             '192.168.8.105', '192.168.10.7', '192.168.10.200',
             '192.168.50.61', '192.168.88.8', '192.168.10.232',
             '192.168.9.127', '192.168.10.183', '192.168.50.73',
             '192.168.50.101', '192.168.10.205', '192.168.9.126',
             '192.168.2.177', '192.168.1.84', '192.168.11.66',
             '192.168.8.50', '192.168.8.51', '192.168.1.76',
             '192.168.10.174', '192.168.10.137', '192.168.10.186',
             '192.168.1.5']

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
