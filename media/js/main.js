    // main.js
    var app = angular.module('myApp', ['ngGrid']);

    app.controller('MyCtrl', function($scope, $timeout) {
    $scope.options = [{ name: "DNSLog", id: 1 }, { name: "Omniture", id: 2 }];
    $scope.selectedOption = $scope.options[0];

    if($scope.selectedOption.name == "DNSLog")
    {
        $scope.type = "DNSLog";
        $scope.dimensions = [
            { field: "date", visible: true, groupable: true },
            { field: "source_ip", visible: true, groupable: true },
            { field: "domainname", visible: true, groupable: true }
        ];

        $scope.metrics = [
            { field: "hits", visible: true, groupable: false },
            { field: "bytes_downloaded", visible: true, groupable: false },
            { field: "bytes_uploaded", visible: true, groupable: false }
        ];
    }
    if($scope.selectedOption.name == "Omniture")
    {
        $scope.type = "Omniture";
        $scope.dimensions = [
            { field: "date", visible: true, groupable: true },
            { field: "site", visible: true, groupable: true },
            { field: "url", visible: true, groupable: true },
            { field: "item", visible: true, groupable: true },
            { field: "position", visible: true, groupable: false }
        ];
        $scope.metrics = [
            { field: "pageview", visible: true, groupable: false },
            { field: "click", visible: true, groupable: false }
        ];
    }

    $scope.fields = $.merge($scope.dimensions, $scope.metrics)
    //alert($scope.fields);

var groupTemplate= '<div ng-click="row.toggleExpand()" ng-style="rowStyle(row)" class="ngAggregate"> <span class="ngAggregateText">{{row.label CUSTOM_FILTERS}} ' +
                                    '(count: {{row.totalChildren()}} {{aggFC(row)}})</span> <div class="{{row.aggClass()}}"></div> </div>';

    $scope.myData;
    $scope.colDefs1 = $scope.fields;

    $scope.columnsSelected = $scope.colDefs1;
    $scope.aggFC = function (row) {
    var vals = 0;

    var calculateChildrenHits = function(cur) {
      var vals = 0;
      var val;
      angular.forEach(cur.children, function(a) {
        val = a.getProperty('hits');
        if (val) { vals += val; }
      });
      return vals;
    };

    var calculateChildrenbytesDownload = function(cur) {
      var vals = 0;
      var val;
      angular.forEach(cur.children, function(a) {
        val = a.getProperty('bytes_downloaded');
        if (val) { vals += val; }
      });
      return vals;
    };

    var calculateChildrenbytesUpload = function(cur) {
      var vals = 0;
      var val;
      angular.forEach(cur.children, function(a) {
        val = a.getProperty('bytes_uploaded');
        if (val) { vals += val; }
      });
      return vals;
    };

    var calculatePageView = function(cur) {
      var vals = 0;
      var val;
      angular.forEach(cur.children, function(a) {
        val = a.getProperty('pageview');
        if (val) { vals += val; }
      });
      return vals;
    };

    var calculateClick = function(cur) {
      var vals = 0;
      var val;
      angular.forEach(cur.children, function(a) {
        val = a.getProperty('click');
        if (val) { vals += val; }
      });
      return vals;
    };

    var calculateAggChildren = function(cur) {
      var hits = 0;
      var bytesD = 0;
      var bytesU = 0;
      var pageview = 0;
      var click = 0;
      if($scope.type == "DNSLog")
      {
          hits += calculateChildrenHits(cur);
          bytesD += calculateChildrenbytesDownload(cur);
          bytesU += calculateChildrenbytesUpload(cur);
          angular.forEach(cur.aggChildren, function(a) {
            calculateAggChildren(a)
            hits += calculateChildrenHits(a);
            bytesD += calculateChildrenbytesDownload(a);
            bytesU += calculateChildrenbytesUpload(a);
          });
          return "Total Hits:" + hits + " Total Bytes Downloaded: " + bytesD + " Total Bytes Uploaded: " + bytesU;
      }
      if($scope.type == "Omniture")
      {
          pageview += calculatePageView(cur);
          click += calculateClick(cur);
          angular.forEach(cur.aggChildren, function(a) {
            calculateAggChildren(a)
            pageview += calculatePageView(a);
            click += calculateClick(a);
          });
          return "Total PageViews:" + pageview + " Total Clicks: " + click;
      }
    };

    return calculateAggChildren(row);
  }

    $scope.gridOptions = {
        data: 'myData',
        enableColumnResize: true,
        showFilter: true,
        enableColumnReordering: true,
        filterOptions: {filterText: '', useExternalFilter: false},
        enablePinning: true,
        aggregateTemplate:groupTemplate,
        columnDefs: 'columnsSelected',
    // selectedItems: $scope.mySelections,
        showGroupPanel: true,
      //  multiSelect: false
    };
    if($scope.selectedOption.name == "DNSLog")
    {
        $scope.type = "DNSLog";
        d3.json("/modeldata/" + $scope.selectedOption.name, function(error, frequency_list) {
        $scope.myData = [];
        $timeout(function(){
        $scope.myData = frequency_list;
        }, 5);
        });
    }
    if($scope.selectedOption.name == "Omniture")
    {
        $scope.type = "Omniture";
        d3.json("../media/js/data/disney_omniture.json", function(error, frequency_list) {
        $scope.myData = [];
        $scope.dimensions = [];
        $scope.metrics = [];
        $scope.fields = [];
        $scope.dimensions = [
            { field: "date", visible: true, groupable: true },
            { field: "site", visible: true, groupable: true },
            { field: "url", visible: true, groupable: true },
            { field: "item", visible: true, groupable: true },
            { field: "position", visible: true, groupable: false }
        ];

        $scope.metrics = [
            { field: "pageview", visible: true, groupable: false },
            { field: "click", visible: true, groupable: false }
        ];
        $scope.fields = $.merge($scope.dimensions, $scope.metrics)
        $scope.colDefs1 = $scope.fields;
        $scope.columnsSelected = $scope.colDefs1;
        $timeout(function(){
        $scope.myData = frequency_list;
        }, 5);
        });
    }

    $scope.LoadGrid = function () {
    if($scope.selectedOption.name == "DNSLog")
    {
        $scope.type = "DNSLog";
        $scope.myData = [];
        $scope.dimensions = [];
        $scope.metrics = [];
        $scope.fields = [];
        $scope.dimensions = [
            { field: "date", visible: true, groupable: true },
            { field: "source_ip", visible: true, groupable: true },
            { field: "domainname", visible: true, groupable: true }
        ];

        $scope.metrics = [
            { field: "hits", visible: true, groupable: false },
            { field: "bytes_downloaded", visible: true, groupable: false },
            { field: "bytes_uploaded", visible: true, groupable: false }
        ];
        $scope.fields = $.merge($scope.dimensions, $scope.metrics)
        $scope.colDefs1 = $scope.fields;
        $scope.columnsSelected = $scope.colDefs1;
        d3.json("/modeldata/" + $scope.selectedOption.name, function(error, frequency_list) {
        $scope.myData = [];
        $timeout(function(){
        $scope.myData = frequency_list;
        }, 5);
        });
    }
    if($scope.selectedOption.name == "Omniture")
    {
        $scope.type = "Omniture";
        $scope.myData = [];
        $scope.dimensions = [];
        $scope.metrics = [];
        $scope.fields = [];
        $scope.dimensions = [
            { field: "date", visible: true, groupable: true },
            { field: "site", visible: true, groupable: true },
            { field: "url", visible: true, groupable: true },
            { field: "item", visible: true, groupable: true },
            { field: "position", visible: true, groupable: false }
        ];

        $scope.metrics = [
            { field: "pageview", visible: true, groupable: false },
            { field: "click", visible: true, groupable: false }
        ];
        $scope.fields = $.merge($scope.dimensions, $scope.metrics)
        $scope.colDefs1 = $scope.fields;
        $scope.columnsSelected = $scope.colDefs1;
        d3.json("../media/js/data/disney_omniture.json", function(error, frequency_list) {
        $timeout(function(){
        $scope.myData = frequency_list;
        }, 5);
        });
    }
    }
    });