    // main.js
    var app = angular.module('predictionApp', ['ngGrid']);

    app.controller('PredictionCtrl', function($scope, $timeout) {

    //$scope.fields = $.merge($scope.dimensions, $scope.metrics)
    //alert($scope.fields);
    $scope.fields = [
            { field: "date", displayName: "Date", visible: true, groupable: false },
            { field: "lastweek", displayName: "Predict with last week data", visible: true, groupable: false, cellTemplate: '<div style="background-color:{{row.entity.rgb}}" ><div class="ngCellText">{{row.getProperty(col.field)}}</div></div>' },
            { field: "onemonth", displayName: "Predict with one month data", visible: true, groupable: false },
            { field: "twomonth", displayName: "Predict with two months data", visible: true, groupable: false },
            { field: "overall",  displayName: "Predict with full data", visible: true, groupable: false },
            { field: "actual", displayName: "Actual Hits data", visible: true, groupable: false }
        ];

var groupTemplate= '<div ng-click="row.toggleExpand()" ng-style="rowStyle(row)" class="ngAggregate"> <span class="ngAggregateText">{{row.label CUSTOM_FILTERS}} ' +
                                    '(count: {{row.totalChildren()}} {{aggFC(row)}})</span> <div class="{{row.aggClass()}}"></div> </div>';

    $scope.myData;
    $scope.colDefs1 = $scope.fields;

    $scope.columnsSelected = $scope.colDefs1;


    $scope.gridOptions = {
        data: 'myData',
        enableColumnResize: true,
        showFilter: true,
        enableColumnReordering: true,
        filterOptions: {filterText: '', useExternalFilter: false},
        enablePinning: true,
      //  aggregateTemplate:groupTemplate,
        columnDefs: 'columnsSelected',
    // selectedItems: $scope.mySelections,
        showGroupPanel: true,
      //  multiSelect: false
    };
        //$scope.type = "DNSLog";
        d3.json("/getprediction/", function(error, frequency_list) {
        $scope.myData = [];
        $timeout(function(){
        $scope.myData = frequency_list;
        }, 5);
        });
    });