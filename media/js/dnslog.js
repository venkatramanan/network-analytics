
function toggle(divId){
    runEffect(divId);
    return false;
}

function runEffect(divId) {
    var selectedEffect = "blind";
    var options = {};
    var elem = document.getElementById(divId+'-button');
    elem.className = (elem.className == "min-max minimize")?"min-max maximize":"min-max minimize";
    $( "#"+divId+"-expand" ).toggle( selectedEffect, options, 500 );
};

function toggleindividual(divId, checkedstate){
    runEffectindividual(divId, checkedstate);
    return false;
}

function runEffectindividual(divId, checkedstate) {
    var selectedEffect = "blind";
    var options = {};
    var elem = document.getElementById(divId+'-button');
    if(checkedstate == true)
    {
        elem.className = "min-max maximize";
        var id = divId+"-expand";
        $( "#"+divId+"-expand" ).hide(500);
        }
    if(checkedstate == false)
   {
        elem.className = "min-max minimize";
        $( "#"+divId+"-expand" ).show(500);
        }
    //elem.className = (elem.className == "min-max minimize")?"min-max maximize":"min-max minimize";
    //$( "#"+divId+"-expand" ).toggle( selectedEffect, options, 500 );
};

