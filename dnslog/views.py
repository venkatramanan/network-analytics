from django.shortcuts import render
from django.http import HttpResponse
from datetime import date, timedelta
from django.db.models import Sum, Count
from django.utils import simplejson
from django.core.serializers.json import DjangoJSONEncoder
from dnslog.models import *
from django.conf import settings
import json
import datetime
import pyRserve
import operator
import requests


def home(request):
    report_date = _get_reportdate_()
    return render(request, 'home.html', {'report_date': report_date})


def BytesDownload(request):
    report_date = _get_reportdate_()
    return render(request, 'BytesDownload.html', {'report_date': report_date})


def LinearPrediction(request):
    return render(request, 'LinearPrediction.html', None)


def splunkdata(request):
    return render(request, 'splunkdata.html', None)


def toyprediction(request):
    return render(request, 'ToysPrediction.html', None)


def trend(request):
    report_date = _get_reportdate_()
    return render(request, 'trendchart.html', {'report_date': report_date})


def cloud(request):
    report_date = _get_reportdate_()
    return render(request, 'domaincloud.html', {'report_date': report_date})


def bubble(request):
    report_date = _get_reportdate_()
    return render(request, 'HitsBubbleReport.html', {'report_date':
                                                            report_date})


def circle(request):
    report_date = _get_reportdate_()
    return render(request, 'circle.html', {'report_date': report_date})


def zoomabletree(request):
    report_date = _get_reportdate_()
    return render(request, 'zoomabletree.html', {'report_date': report_date})


def Getcircledata(request, date):
    get_result = []
    report_data = ConsolidatedCategoryCircle.objects.filter(
        date=date).values("category").order_by("-category_percent")
    category_list = []
    for data in report_data:
        if data not in category_list and len(category_list) <= 9:
            category_list.append(data)

    for report in category_list:
        result_data = ConsolidatedCategoryCircle.objects.filter(
            category=report['category'], date=date).order_by('-hits')[:9]
        hits_data = [{'name': data.domainname,
                'size': "{0:.2f}".format(data.hits_percent)}
                for data in result_data]
        cat_percent = "{0:.2f}".format(result_data[0].category_percent)
        report_data = _add_others_category_chart_(hits_data, 'size',
                                                                cat_percent)
        domain_child = [{'name': report['category'], 'size': cat_percent,
                                                    'children': report_data}]
        get_result.extend(domain_child)
    if get_result:
        json_data = {'name': 'categories', 'size': '', 'children': get_result}
    else:
        json_data = {'name': 'No data', 'size': '', 'children': get_result}
    result = json.dumps(json_data)
    return HttpResponse(result, content_type="application/json")


def comparision(request):
    if request.method == "POST":
        report_date = request.POST.get('dashboard-date', '')
    else:
        report_date = _get_reportdate_()
    employee = []
    SkypeEmployee = _gettopIndividualempids_(IndividualComparisionChart,
    "Skype", report_date, 5)
    LyncEmployee = _gettopIndividualempids_(IndividualComparisionChart,
        "Lync", report_date, 5)
    employee.extend(SkypeEmployee)
    employee.extend(LyncEmployee)
    return render(request, 'comparisionPie.html',
                                    {'result': simplejson.dumps(employee),
                                     'report_date': report_date, 'num': 10})


def _gettopIndividualempids_(modelObject, domainname, report_date, n):
    report_data = modelObject.objects.filter(date=report_date, domain=
        domainname).filter(source_ip__startswith='192.168').exclude(
            source_ip__in=settings.NOMAN_IPS).values('source_ip','overall_bytes').order_by(
                    '-overall_bytes')[:n]
    source_ip = [data['source_ip'] for data in report_data if data['overall_bytes'] > 512]
    employee = []
    for ip in source_ip:
        data = SourceIP.objects.filter(ip_address=ip)
        if data:
            data = data[0]
            employee.append({'empid': int(data.employee_id),
                'photo': data.photos if data.photos else int(data.employee_id),
                  'domain': domainname,
                  'empName': data.user_name})
        else:
            employee.append({'empid': ip,
                    'photo': ip,
                  'domain': domainname, 'empName': ''})
    print employee
    return employee


def GetIndividualComparisonData(request, domain, empid, date):
    report_data = IndividualComparisionChart.objects.filter(date=
                  date, domain=domain).filter(source_ip=_getipaddress_(empid))

    get_avg_report_data = IndividualComparisionChart.objects.filter(date=date)
    max_value = IndividualComparisionChart.objects.filter(date=date, domain=
    domain).values('overall_bytes').order_by('-overall_bytes')[:1]
    skypeCount = 0
    lyncCount = 0
    overallSkypeSum = 0
    overallLyncSum = 0
    for d in get_avg_report_data:
        if d.overall_bytes > 0:
            if d.domain == "skype":
                skypeCount += 1
                overallSkypeSum += (float(d.overall_bytes / 1024))
            else:
                lyncCount += 1
                overallLyncSum += (float(d.overall_bytes / (1024 * 1024)))
    data = [{"title": ["("+"{0:.1f}".format(round(float(data.overall_bytes / 1024)),1) + " KB) "] if domain == "Skype" else
                    ["("+"{0:.1f}".format(round(float(data.overall_bytes) / (1024 * 1024)),1) + " MB) "],
             "ranges": ["{0:.2f}".format(float(max_value[0]['overall_bytes']) / 1024)]
                        if domain == "Skype" else
                      ["{0:.2f}".format(float(max_value[0]['overall_bytes']) / (1024 * 1024))],
             "measures": ["{0:.2f}".format(float(data.overall_bytes / 1024))] if domain == "Skype"
                        else ["{0:.2f}".format(float(data.overall_bytes) / (1024 * 1024))],
             "markers": ["{0:.2f}".format(float(overallSkypeSum) / skypeCount)]
                       if domain == "Skype" else ["{0:.2f}".format(float(overallLyncSum) /
                                                       lyncCount)]}
            for data in report_data if data.overall_bytes > 512]
    result = json.dumps(data)
    return HttpResponse(result, content_type="application/json")


def Getcomparisiondata(request, date):
    overall_count = 0.0
    report_data = DomainComparisionChart.objects.filter(
        date=date)
    for data in report_data:
        overall_count += float((data.downloaded_bytes) + (data.uploaded_bytes))
    data = [{"label":data.domain,
                  "value":"{0:.2f}".format((((data.downloaded_bytes)
                  + (data.uploaded_bytes)) / overall_count) * 100),
                  "date": str(data.date),
                  "Totalbytes":(data.downloaded_bytes + data.uploaded_bytes)}
                      for data in report_data if (data.downloaded_bytes + data.uploaded_bytes) > 512]
    result = json.dumps(data)
    return HttpResponse(result, content_type="application/json")


def Gettrendchart(request):
    report_data = DomainComparisionChart.objects.order_by('-date')[:15]
    data = [{"domain":data.domain + " in KB" if data.domain == 'skype'
    else data.domain + " in MB",
             "date": str(data.date),
            "overall_bytes":-(data.downloaded_bytes + data.uploaded_bytes) / 1024
                  if data.domain == "skype" else
                  (data.downloaded_bytes + data.uploaded_bytes) / (1024 * 1024)}
                      for data in report_data]
    result = json.dumps(data)
    return HttpResponse(result, content_type="application/json")


def angular(request):
    return render(request, 'angularsample.html')


def individualhits(request):
    if request.method == "POST":
        report_date = request.POST.get('dashboard-date', '')
        n = request.POST.get('ddlLimit', 5)
    else:
        report_date = _get_reportdate_()
        n = 5
    employee = _gettopNempids_(IndividualHits, report_date, n)
    return render(request, 'IndividualConsumption.html',
                                    {'result': simplejson.dumps(employee),
                                     'report_date': report_date, 'num': n})


def _gettopNempids_(modelObject, report_date, n):
    global avgHitsPercentage
    report_data = modelObject.objects.filter(date=report_date).filter(
            source_ip__startswith='192.168').exclude(source_ip__in=
            settings.NOMAN_IPS).values('source_ip').annotate(totalhits=
            Sum('hits')).order_by('-totalhits')[:n]

    avgHitsCount = __calculateAvgHitConsumption__(report_date)
    avgHitsPercentage = "{0:.2f}".format(float(avgHitsCount) / float(
                                                totalConsolidatedHits) * 100)
    source_ip = [{'source_ip': data['source_ip'],
                    'totalHits': data['totalhits']} for data in report_data]
    employee = []
    for ip in source_ip:
        data = SourceIP.objects.filter(ip_address=ip['source_ip'])
        individualHitPer = 0
        if modelObject.__name__ == 'IndividualHits':
            individualHits = (ip['totalHits']) - (avgHitsCount)
            individualHitPer = "{0:.2f}".format(float(individualHits) / float(
                                                totalConsolidatedHits) * 100)
        arrowImg = "Icon-Moreviewed-Orange.png" if individualHitPer >  avgHitsPercentage else "Icon-Moreviewed-Green.png"
        if data:
            data = data[0]
            employee.append({'empid': int(data.employee_id),
                  'photo': data.photos if data.photos else 'no-image.jpeg',
                  'empName': data.user_name,
                  'hitPerIP': individualHitPer,
                  'hitPerPhoto': arrowImg})
        else:
            employee.append({'empid': ip['source_ip'],
                                  'photo': 'no-image.jpeg', 'empName': '',
                              'hitPerIP': individualHitPer,
                              'hitPerPhoto': arrowImg})

    return employee


def __calculateAvgHitConsumption__(report_date):
    global avgHitsCount
    global totalConsolidatedHits
    # AVG Total Hits
    overallHitsDetails = ConsolidateHits.objects.filter(
                                date=report_date).values('date').annotate(
                                totalConsolidatedHits=Sum('hits'))

    source_ip_count = IndividualHits.objects.filter(date=report_date).filter(
            source_ip__startswith='192.168').exclude(source_ip__in=
            settings.NOMAN_IPS).values('date').annotate(ipCount=
                                    Count('source_ip', distinct=True))
    totalConsolidatedHits = overallHitsDetails[0]['totalConsolidatedHits']
    ipcount = source_ip_count[0]['ipCount']
    avgHitsCount = totalConsolidatedHits / ipcount
    return avgHitsCount


def GetConsolidateHits(request, date):
    query_data = ConsolidateHits.objects.filter(date=
                    date).order_by('-hits')[:9]
    data = [{'domainname':data.domainname,
             'per':"{0:.2f}".format(data.hitspercent)}
                            for data in query_data]
    report_data = _add_others_per_(data, 'per')
    result = json.dumps(report_data)
    return HttpResponse(result, content_type="application/json")


def GetConsolidateHitsForBubble(request, date):
    query_data = ConsolidateHits.objects.filter(date=
                    date).order_by('-hits')[:50]
    data = [{'domainname':data.domainname,
             'per':"{0:.2f}".format(data.hitspercent)}
                            for data in query_data]
    result = json.dumps(data)
    return HttpResponse(result, content_type="application/json")


def Getclouddata(request, date):
    query_data = ConsolidateHits.objects.filter(date=
                    date).order_by('-hits')[:50]
    data = [{'domainname':data.domainname,
             'per':"{0:.2f}".format(data.hitspercent)}
                            for data in query_data]
    result = json.dumps(data)
    return HttpResponse(result, content_type="application/json")


def Gettrenddata(request):
    report_data = ConsolidateHits.objects.values('date').annotate(
                                totalhits=Sum('hits')).order_by('-date')[:7]
    hits_data = [{'date':'{0.month}/{0.day}/{0.year}'.format(data['date']),
             'type':'hits',
             'totalhits':data['totalhits']}
                for data in report_data]
    result = json.dumps(hits_data)
    return HttpResponse(result, content_type="application/json")


def GetConsolidateDownloadBytes(request, date):
    report_data = ConsolidateDownload.objects.filter(date=
                    date).order_by('-bytesdownloaded')[:9]
    data = [{'domainname':data.domainname,
                          'per':"{0:.2f}".format(data.bytesdownloadedpercent)}
                          for data in report_data]
    result = json.dumps(_add_others_per_(data, 'per'))
    return HttpResponse(result, content_type="application/json")


def GetIndividualHits(request, empid, date):
    report_data = IndividualHits.objects.filter(date=
                  date).filter(source_ip=_getipaddress_(empid)).order_by(
                        '-hits')[:9]
    data = [{'domainname':data.domainname,
                          'per':"{0:.2f}".format(data.hitspercent)}
                           for data in report_data]
    result = json.dumps(_add_others_per_(data, 'per'))
    return HttpResponse(result, content_type="application/json")


def GetConsolidatedObjectData(request, obj):
    if(obj == "DNSLog"):
        report_data = IndividualHitsData.objects.all().order_by('-date')[:50000]
        result = json.dumps(list(report_data.values('date', 'source_ip',
                                                    'domainname', 'hits',
                                                    'bytes_downloaded',
                                                    'bytes_uploaded')),
                                                    cls=DjangoJSONEncoder)
        return HttpResponse(result, content_type="application/json")


def report(request):
    result = None
    return render(request, 'report.html', {'result': result})


def GetConsolidateCntPer(request):
    report_date = _get_reportdate_()
    query_data = ConsolidateHits.objects.filter(date=
                    report_date).order_by('-hits')[:9]
    report_data = [{'cnt':data.hits, 'per':data.hitspercent}
                    for data in query_data]
    result = json.dumps(report_data)
    return HttpResponse(result, content_type="application/json")


def getConsolidatedCategoryDetails(request, date):
    report_data = ConsolidatedCategory.objects.filter(date=
                                        date).order_by('-hits')[:9]
    data = [{'category': data.category,
                'percentage':"{0:.2f}".format(data.hitspercent)}
                          for data in report_data]
    per_sum = 0.0
    for d in data:
        per_sum += float(d['percentage'])
    data.append({'category': 'Others',
                    'percentage': "{0:.2f}".format(100.0 - per_sum)})
    result = json.dumps(data)
    return HttpResponse(result, content_type="application/json")


def individual(request):
    if request.method == "POST":
        report_date = request.POST.get('dashboard-date', '')
        n = request.POST.get('ddlLimit', 5)
    else:
        report_date = _get_reportdate_()
        n = 5
    employeeInHits = _gettopNempids_(IndividualHits, report_date, n)
    employeeInCategory = _gettopNempids_(IndividualCategory, report_date, n)
    return render(request, 'individualreport.html',
                                    {'result': simplejson.dumps(employeeInHits),
                   'categoryemployeedata': simplejson.dumps(employeeInCategory),
                    'hitsemployeedata': simplejson.dumps(employeeInHits),
                    'report_date': report_date, 'num': n,
                    'avgHitsCount': avgHitsCount,
                    'totalConsolidatedHits': totalConsolidatedHits,
                    'avgHitsPercentage': avgHitsPercentage})


def GetIndividualCategory(request, empid, date):
    report_data = IndividualCategory.objects.filter(date=
                  date).filter(source_ip=_getipaddress_(empid)).order_by(
                          '-hits')[:9]
    data = [{'category':data.category,
             'per':"{0:.2f}".format(data.hitspercent)}
                           for data in report_data]
    categorytypes = CategoryLookup.objects.values('name', 'type')
    per_sum = 0.0
    for d in data:
        per_sum += float(d['per'])
    data.append({'category': 'Others',
                        'per': "{0:.2f}".format(100.0 - per_sum)})
    category_data = []
    for value in data:
        for types in categorytypes:
            if types['name'] == value['category']:
                value['type'] = types['type']
        category_data.append(value)
    result = json.dumps(category_data)
    return HttpResponse(result, content_type="application/json")


def GetLinearPrediction(request, duration):
    print(duration)
    conn = pyRserve.connect(host='192.168.8.104', port=6311)
    conn.eval('library(lmmodel)')
    if duration == "LastWeek":
        result = conn.eval('GetLastweek()')
    elif duration == "LastMonth":
        result = conn.eval('GetLastMonth()')
    elif duration == "TwoMonths":
        result = conn.eval('GetLastTwoMonth()')
    else:
        result = conn.eval('GetOverall()')
    conn.close()
    #r = json.dumps(result['date'])
    return HttpResponse(result, content_type="application/json")


def GetSalesPrediction(request):
    conn = pyRserve.connect(host='192.168.8.104', port=6311)
    conn.eval('library(preToys)')
    conn.eval('data<-getData()')
    yearWisesales = conn.eval('getYearsales(data)')
    conn.close()
    return HttpResponse(yearWisesales, content_type="application/json")


def GetCustomerPrediction(request):
    conn = pyRserve.connect(host='192.168.8.104', port=6311)
    conn.eval('library(preToys)')
    conn.eval('data<-getData()')
    yearWisesales = conn.eval('getYearCustomer(data)')
    conn.close()
    return HttpResponse(yearWisesales, content_type="application/json")


def GetMonthWiseSalesPrediction(request):
    conn = pyRserve.connect(host='192.168.8.104', port=6311)
    conn.eval('library(preToys)')
    conn.eval('data<-getData()')
    monthwisesales = conn.eval('getMonthyearsales(data)')
    conn.close()
    a = json.loads(monthwisesales)
    b = [elem for elem in a if elem['purchasedate'].startswith('2014')]
    final_data = []
    for data in b:
        purdate = data['purchasedate']
        datef = datetime.datetime.strptime(purdate, '%Y-%m-%d').date()
        monthf = datetime.datetime.strftime(datef, '%b')
        final_data.append({'purchasedate': purdate,
                            'Month': monthf,
                            'category': data['category'],
                            'Sales': data['Sales']})
    result = json.dumps(final_data)
    return HttpResponse(result, content_type="application/json")


def GetPredictionReport(request):
    report_data = HitsPredictionData.objects.all().order_by('-date')
    result = list(report_data.values('date', 'lastweek',
                                                    'onemonth', 'twomonth',
                                                    'overall'))
    predicted_dates = [d.date for d in report_data]
    actual_datas = list(ConsolidateHits.objects.filter(date__in=
                                predicted_dates).values('date').annotate(
                                totalhits=Sum('hits')))
    final_data = []
    for predicted_date in predicted_dates:
        predicted_data = [i for i in result if i['date'] == predicted_date]
        actual_data = [j for j in actual_datas if j['date'] == predicted_date]
        predicted_lastweek_data = predicted_data[0]['lastweek']
        predicted_onemonth_data = predicted_data[0]['onemonth']
        predicted_twomonth_data = predicted_data[0]['twomonth']
        predicted_overall_data = predicted_data[0]['overall']
        if len(actual_data) == 0:
            actual_hits = "Yet to find"
            accuracy_lastweek = "Yet to find"
            accuracy_onemonth = "Yet to find"
            accuracy_twomonth = "Yet to find"
            accuracy_overall = "Yet to find"
        if len(actual_data) > 0:
            actual_hits = actual_data[0]['totalhits']
            accuracy_lastweek = _findaccuracy(predicted_lastweek_data,
                                                actual_hits)
            accuracy_onemonth = _findaccuracy(predicted_onemonth_data,
                                                actual_hits)
            accuracy_twomonth = _findaccuracy(predicted_twomonth_data,
                                                actual_hits)
            accuracy_overall = _findaccuracy(predicted_overall_data,
                                                actual_hits)
        final_data.append({'date': '{0.month}/{0.day}/{0.year}'.format(
                            predicted_date),
                            'lastweek': str(predicted_lastweek_data) + "(" +
                                                str(accuracy_lastweek) + ")",
                             'onemonth': str(predicted_onemonth_data) + "(" +
                                                 str(accuracy_onemonth) + ")",
                              'twomonth': str(predicted_twomonth_data) + "(" +
                                                  str(accuracy_twomonth) + ")",
                              'overall': str(predicted_overall_data) + "(" +
                                                  str(accuracy_overall) + ")",
                               'actual': actual_hits})
    #final_data.sort(key=operator.itemgetter('date'))
    result = json.dumps(final_data)
    return HttpResponse(result, content_type="application/json")


def _findaccuracy(predicted, actual):
    predicted = int(predicted)
    actual = int(actual)
    if(predicted > actual):
        numerator = predicted - actual
    else:
        numerator = actual - predicted
    #print (int(numerator) / float(actual))
    return "{0:.2f}%".format((numerator / float(actual)) * 100)


def _getipaddress_(empid):
    # if ip returning the ip address
    if empid.find('.') > 0:
        return empid

    #if employee id then returning ip-address
    empdetails = SourceIP.objects.filter(employee_id=empid)
    r = list(empdetails[:1])
    if r:
        return r[0].ip_address
    return ""


def _getDomainModel_():
    return DestinationIP.objects.all()


def _getDomainName_(data, ip):
    domain_details = data.filter(ip_address=ip)
    r = list(domain_details[:1])
    if r:
        return r[0].Domain_friendly_name
    return ip


def _get_reportdate_():
    data = ImportLog.objects.filter(IsSuccess=1).order_by("-pk")
    date_value = None
    if data:
        date_value = data[0].Log_Date
        date_value = '%s-%s-%s' % (str(data[0].Log_Date[:4]),
                            str(data[0].Log_Date[4:6]),
                            str(data[0].Log_Date[6:8]))
    else:
        date_value = (date.today() - timedelta(days=1)).isoformat()
    return date_value


def help(request):
    result = None
    return render(request, 'help.html', {'result': result})


def _add_others_per_(data, key):
    per_sum = 0.0
    for d in data:
        per_sum += float(d[key])
    data.append({'dip': '0.0.0.0', 'domainname': 'Others',
                        key: "{0:.2f}".format(100.0 - per_sum)})
    return data


def _add_others_category_chart_(hits_data, key, cat_percent):
    domain_percent_sum = 0.0
    for d in hits_data:
        domain_percent_sum += float(d[key])
    hits_data.append({'name': 'Others', 'hits': 0,
        'category_percent': 0.0, key: "{0:.2f}".format(float(cat_percent) - domain_percent_sum),
            })
    return hits_data


def hashtags(request):
    report_date = requests.get(settings.IMPALA_API_URL +
                                                  settings.API_URL_REPORT_DATE)
    adidashashtags_URL = settings.IMPALA_API_URL + settings.API_URL_HASHTAGSADIDAS
    nikehashtags_URL = settings.IMPALA_API_URL + settings.API_URL_HASHTAGSNIKE
    reebokhashtags_URL = settings.IMPALA_API_URL + settings.API_URL_HASHTAGSREEBOK
    return render(request, 'Hashtag.html', {
        'report_date': report_date.text,
            'adidashashtags_URL': adidashashtags_URL,
	    'nikehashtags_URL': nikehashtags_URL,
	    'reebokhashtags_URL': reebokhashtags_URL})


def wordcloud(request):
    report_date = requests.get(settings.IMPALA_API_URL +
                                                  settings.API_URL_REPORT_DATE)
    adidaswordcloud_URL = settings.IMPALA_API_URL + settings.API_URL_WORDCLOUDADIDAS
    nikewordcloud_URL = settings.IMPALA_API_URL + settings.API_URL_WORDCLOUDNIKE
    reebokwordcloud_URL = settings.IMPALA_API_URL + settings.API_URL_WORDCLOUDREEBOK
    return render(request, 'Wordcloud.html', {
        'report_date': report_date.text,
            'adidaswordcloud_URL': adidaswordcloud_URL,
            'nikewordcloud_URL': nikewordcloud_URL,
            'reebokwordcloud_URL': reebokwordcloud_URL})


def twitterzoomabletree(request):
    report_date = requests.get(settings.IMPALA_API_URL +
                                                  settings.API_URL_REPORT_DATE)
    twitterzoomabletree_URL = settings.IMPALA_API_URL + settings.API_URL_TWITTERSENTIMENT
    return render(request, 'twitterzoomabletree.html', {
        'report_date': report_date.text,
            'twitterzoomabletree_URL': twitterzoomabletree_URL})

def retweet(request):
    report_date = requests.get(settings.IMPALA_API_URL +
                                                  settings.API_URL_REPORT_DATE)
    retweet_URL = settings.IMPALA_API_URL + settings.API_URL_RETWEET
    return render(request, 'retweet.html', {
        'report_date': report_date.text,
            'retweet_URL': retweet_URL})
