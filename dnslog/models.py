from django.db import models

# Create your models here.


class IndividualHits(models.Model):
    id = models.AutoField(primary_key=True)
    source_ip = models.CharField(max_length=20, null=True)
    #username = models.CharField(max_length=100, null=True)
    domainname = models.CharField(max_length=20, null=True)
    hits = models.BigIntegerField(default=0, null=True)
    hitspercent = models.FloatField(default=0.0, null=True)
    date = models.CharField(max_length=32, null=True)

    class Meta:
        verbose_name = "individual_consumption_hits"
        db_table = "individual_consumption_hits"


class IndividualUpload(models.Model):
    id = models.AutoField(primary_key=True)
    source_ip = models.CharField(max_length=20, null=True)
    #username = models.CharField(max_length=20, null=True)
    destination_ip = models.CharField(max_length=20, null=True)
    bytesuploaded = models.BigIntegerField(default=0, null=True)
    bytesuploadedpercent = models.FloatField(default=0.0, null=True)
    date = models.CharField(max_length=32, null=True)

    class Meta:
        verbose_name = "individual_consumption_bytes_uploaded"
        db_table = "individual_consumption_bytes_uploaded"


class IndividualDownload(models.Model):
    id = models.AutoField(primary_key=True)
    source_ip = models.CharField(max_length=20, null=True)
    #username = models.CharField(max_length=20, null=True)
    destination_ip = models.CharField(max_length=20, null=True)
    bytesdownloaded = models.BigIntegerField(default=0, null=True)
    bytesdownloadedpercent = models.FloatField(default=0.0, null=True)
    date = models.CharField(max_length=32, null=True)

    class Meta:
        db_table = "individual_consumption_bytes_downloaded"
        verbose_name = "individual_consumption_bytes_downloaded"


class ConsolidateHits(models.Model):
    id = models.AutoField(primary_key=True)
    domainname = models.CharField(max_length=20, null=True)
    hits = models.BigIntegerField(default=0, null=True)
    hitspercent = models.FloatField(default=0.0, null=True)
    date = models.CharField(max_length=32, null=True)

    class Meta:
        db_table = "consolidated_consumption_hits"
        verbose_name = "consolidated_consumption_hits"


class ConsolidateUpload(models.Model):
    destination_ip = models.CharField(max_length=20, null=True)
    bytesuploaded = models.BigIntegerField(default=0, null=True)
    bytesuploadedpercent = models.FloatField(default=0.0, null=True)
    date = models.CharField(max_length=32, null=True)

    class Meta:
        db_table = "consolidated_consumption_bytes_uploaded"
        verbose_name = "consolidated_consumption_bytes_uploaded"


class ConsolidateDownload(models.Model):
    id = models.AutoField(primary_key=True)
    domainname = models.CharField(max_length=20, null=True)
    bytesdownloaded = models.BigIntegerField(default=0, null=True)
    bytesdownloadedpercent = models.FloatField(default=0.0, null=True)
    date = models.CharField(max_length=32, null=True)

    class Meta:
        db_table = "consolidated_consumption_bytes_downloaded"
        verbose_name = "consolidated_consumption_bytes_downloaded"


class DestinationIP(models.Model):
    id = models.AutoField(primary_key=True)
    ip_address = models.CharField(max_length=50, null=True)
    port = models.BigIntegerField(default=0, null=True)
    domainname = models.TextField(null=True)
    Domain_friendly_name = models.TextField(null=True)

    class Meta:
        db_table = "destination_ip_address_new"
        verbose_name = "destination_ip_address_new"


class SourceIP(models.Model):
    id = models.AutoField(primary_key=True)
    ip_address = models.CharField(max_length=50, null=True)
    user_name = models.CharField(max_length=50, null=True)
    system_name = models.CharField(max_length=50, null=True)
    employee_id = models.BigIntegerField(default=0, null=True)
    photos = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "source_ip_address_new"
        verbose_name = "source_ip_address_new"


class ImportLog(models.Model):
    Log_Date = models.CharField(max_length=20, null=True)
    IsSuccess = models.IntegerField()
    ImportDate = models.DateTimeField(primary_key=True)
    Error = models.CharField(max_length=2000, null=True)
    ReportType = models.CharField(max_length=32, null=True)

    class Meta:
        db_table = "mysql_import_log"
        verbose_name = "mysql_import_log"


class IndividualCategory(models.Model):
    id = models.AutoField(primary_key=True)
    source_ip = models.CharField(max_length=20, null=True)
    #username = models.CharField(max_length=100, null=True)
    category = models.CharField(max_length=250, null=True)
    hits = models.BigIntegerField(default=0, null=True)
    hitspercent = models.FloatField(default=0.0, null=True)
    date = models.CharField(max_length=32, null=True)

    class Meta:
        db_table = "individual_consumption_category"
        verbose_name = "individual_consumption_category"


class ConsolidatedCategory(models.Model):
    id = models.AutoField(primary_key=True)
    category = models.CharField(max_length=250, null=True)
    hits = models.BigIntegerField(default=0, null=True)
    hitspercent = models.FloatField(default=0.0, null=True)
    date = models.CharField(max_length=32, null=True)

    class Meta:
        db_table = "consolidated_consumption_category"
        verbose_name = "consolidated_consumption_category"


class CategoryLookup(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=250, null=True)
    type = models.CharField(max_length=150, null=True)

    class Meta:
        db_table = "category_lookup"
        verbose_name = "category_lookup"


class IndividualHitsData(models.Model):
    id = models.AutoField(primary_key=True)
    source_ip = models.CharField(max_length=32, null=True)
    domainname = models.CharField(max_length=150, null=True)
    hits = models.BigIntegerField(default=0, null=True)
    bytes_downloaded = models.BigIntegerField(default=0, null=True)
    bytes_uploaded = models.BigIntegerField(default=0, null=True)
    date = models.CharField(max_length=32, null=True)

    class Meta:
        db_table = "individual_domain_bytes"
        verbose_name = "individual_domain_bytes"


class ConsolidatedCategoryCircle(models.Model):
    category = models.CharField(max_length=100, null=True)
    domainname = models.CharField(max_length=100, null=True)
    hits = models.BigIntegerField(default=0, null=True)
    category_percent = models.FloatField(default=0.0, null=True)
    domain_percent = models.FloatField(default=0.0, null=True)
    hits_percent = models.FloatField(default=0.0, null=True)
    date = models.CharField(max_length=32, null=True)
    id = models.AutoField(primary_key=True)

    class Meta:
        db_table = "consolidated_consumption_category_chart"
        verbose_name = "consolidated_consumption_category_chart"


class DomainComparisionChart(models.Model):
    domain = models.CharField(max_length=100, null=True)
    downloaded_bytes = models.BigIntegerField(default=0, null=True)
    uploaded_bytes = models.BigIntegerField(default=0, null=True)
    date = models.CharField(max_length=32, null=True)
    id = models.AutoField(primary_key=True)

    class Meta:
        db_table = "consolidated_comparision_data"
        verbose_name = "consolidated_comparision_data"


class IndividualComparisionChart(models.Model):
    id=models.AutoField(primary_key=True)
    domain = models.CharField(max_length=100,null=True)
    source_ip = models.CharField(max_length=100,null=True)
    downloaded_bytes = models.BigIntegerField(default=0, null=True)
    uploaded_bytes = models.BigIntegerField(default=0,null=True)
    date = models.CharField(max_length=32,null=True)
    overall_bytes = models.BigIntegerField(default=0,null=True)


    class Meta:
        db_table = "individual_comparision_data"
        verbose_name = "individual_comparision_data"

class HitsPredictionData(models.Model):
    id=models.AutoField(primary_key=True)
    date = models.CharField(max_length=32, null=True)
    lastweek = models.BigIntegerField(default=0, null=True)
    onemonth = models.BigIntegerField(default=0, null=True)
    twomonth = models.BigIntegerField(default=0, null=True)
    overall = models.BigIntegerField(default=0, null=True)

    class Meta:
        db_table = "hits_predicted"
        verbose_name = "hits_predicted"



